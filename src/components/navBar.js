import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {Actions} from 'react-native-router-flux';
import {backIcon, shareIcon, searchIcon} from '../assets/images/icon';
const ios = Platform.OS === 'ios';
const android = Platform.OS === 'android';
import {connect} from 'react-redux';

class CustomNavBar extends React.Component {
  _renderLeft() {
    return (
      <TouchableOpacity
        onPress={() => Actions.pop()}
        style={[styles.navBarItem, {paddingLeft: 10}]}>
        {backIcon}
      </TouchableOpacity>
    );
  }

  _renderMiddle() {
    if (Actions.currentScene == 'home') {
      const city =
        (this.props.address &&
          this.props.address.plus_code &&
          this.props.address.plus_code.compound_code.split(',')) ||
        [];
      console.log(city, city);
      return (
        <View style={styles.navBarItem}>
          <Text style={styles.homeTite}>{city[city.length - 2] || ''}</Text>
          <Text style={styles.subTitle}>ukuran 100</Text>
        </View>
      );
    }
    return (
      <View style={styles.navBarItem}>
        <Text>
          {(this.props.selected && this.props.selected.region.regency_name) ||
            ''}
        </Text>
      </View>
    );
  }

  _renderRight() {
    return (
      <View
        style={[
          styles.navBarItem,
          {flexDirection: 'row', justifyContent: 'flex-end'},
        ]}>
        <TouchableOpacity
          onPress={() => console.log('Share')}
          style={{paddingRight: 10}}>
          {shareIcon}
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    const dinamicStyle = {};
    return (
      <View style={[styles.container, dinamicStyle]}>
        {this._renderLeft()}
        {this._renderMiddle()}
        {this._renderRight()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: Platform.OS === 'ios' ? 100 : 80,
    flexDirection: 'row',
    paddingTop: ios ? 50 : 25,
    borderBottomWidth: 1,
    borderColor: '#d6d7da',
  },
  navBarItem: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'baseline',
  },
  homeTite: {
    color: '#c9ccd2',
    fontSize: 20,
    textAlign: 'center',
  },
  subTitle: {
    textAlign: 'center',
  },
});

const mapStateToProps = state => {
  return {
    address: state.rootReducer.address,
    selected: state.rootReducer.item,
  };
};
export default connect(mapStateToProps, null)(CustomNavBar);
