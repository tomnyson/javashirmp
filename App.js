/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from './src/store/store';
import Sceens from './src/scenes/routers';
const store = configureStore();
const App = () => {
  console.log('store', store);
  return (
    <Provider store={store}>
      <Sceens />
    </Provider>
  );
};
export default App;
