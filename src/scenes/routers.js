import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Button,
  Text,
  StatusBar,
} from 'react-native';
import {Router, Scene, Actions} from 'react-native-router-flux';
import HomeScreen from './home';
import DetailScreen from './detail';
import navBar from '../components/navBar';
import SplashScreen from '../components/splashScreen';

class Scenes extends Component {
  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene
            key="splash"
            component={SplashScreen}
            title="home screen"
            initial
            hideNavBar={true}
          />
          <Scene
            key="home"
            component={HomeScreen}
            title="home screen"
            navBar={navBar}
          />
          <Scene
            key="detail"
            component={DetailScreen}
            title="detai screen"
            navBar={navBar}
          />
        </Scene>
      </Router>
    );
  }
}
export default Scenes;
