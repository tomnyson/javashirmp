/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Button,
  View,
  Text,
  StatusBar,
  FlatList,
  TouchableOpacity,
  Dimensions,
  navigator,
  TextInput,
  KeyboardAvoidingView,
  ActivityIndicator,
} from 'react-native';
import {Router, Scene, Actions} from 'react-native-router-flux';
import jalaServices from '../../services/api';
import {
  shareIcon,
  nextIcon,
  fiterIcon,
  sortIcon,
  searchIcon,
  infoIcon,
  checkIcon,
} from '../../assets/images/icon';
import Modal from 'react-native-modal';
import Geolocation from '@react-native-community/geolocation';
import {formatNumber} from '../../utils';
import {connect} from 'react-redux';
import {setItem, setLocation, setAddress} from '../../store/actions';
const deviceHeight =
  Platform.OS === 'ios'
    ? Dimensions.get('window').height
    : Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const extractKey = ({id}) => id;
class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: [],
      isloading: true,
      location: [],
      value: '',
      filter: 'desc',
      selected: null,
      isModalVisible: false,
      isSortModal: false,
      address: null,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.init();
    }, 50);
    Geolocation.getCurrentPosition(async info => {
      const {setLocation, setAddress, address} = this.props;
      setLocation(info);
      if (address == null) {
        const address = await jalaServices.getAddressByLocation(
          info.coords.latitude,
          info.coords.longitude,
        );
        this.setState({
          address: address,
        });
        setAddress(address);
      }
    });
  }

  init = async () => {
    const data = await jalaServices.getAllShirmpPrices();
    const location = await jalaServices.getSearchLocation();
    console.log('location', location);
    this.setState({
      data: data.data,
      loading: false,
      location: location.data,
    });
  };

  toggleModal = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };
  nextDetail = async item => {
    //get Special Id
    const species = await jalaServices.getSpeciesByID(item.species_id);
    const data = {
      ...item,
      species,
    };
    const {setItem} = this.props;
    setItem(data);
    Actions.detail({data});
  };
  renderItem = ({item}) => {
    return item ? (
      <View style={styles.containerItem}>
        <Text style={styles.title}>{item.contact}</Text>
        <View style={styles.content}>
          <View style={styles.left}>
            <Text style={styles.price}>RP {formatNumber(item.size_40)}</Text>
            <Text style={styles.address}>
              {(item.region && item.region.province_name) || ''}
            </Text>
            <Text style={styles.location}>
              {item.creator && new Date(item.creator.created_at).toDateString()}
              {', '}
              oleh {item.creator && item.creator.gender_name}
            </Text>
          </View>
          <View style={styles.right}>
            <View>{shareIcon}</View>
            <TouchableOpacity
              style={styles.next}
              onPress={() => this.nextDetail(item)}>
              <Text style={styles.detail}>detail </Text>
              {nextIcon}
            </TouchableOpacity>
          </View>
        </View>
      </View>
    ) : null;
  };
  handleFilter = item => {
    if (item) {
      this.setState({
        value: item.name,
        selected: item,
      });
    }
  };
  changeFilter = async () => {
    const {selected} = this.state;
    if (selected) {
      const result = await jalaServices.getAllShirmpPrices(
        '',
        selected.id || '',
      );
      console.log('result', result);
      this.setState({
        data: result.data,
        isModalVisible: false,
      });
    }
  };
  renderItemSearch = ({item}) => {
    return item ? (
      <View style={styles.containerSearchItem}>
        <TouchableOpacity onPress={() => this.handleFilter(item)}>
          <Text style={styles.titleSearch}>{item.name}</Text>
        </TouchableOpacity>
      </View>
    ) : null;
  };
  onChangeText = e => {
    this.setState({value: e.toUpperCase()});
    const seft = this;
    if (e.length > 2) {
      setTimeout(async () => {
        const location = await jalaServices.getSearchLocation(e.toUpperCase());
        seft.setState({
          location: location.data,
        });
      }, 50);
    }
  };
  resetFilter = () => {
    this.setState({
      value: '',
      selected: null,
      isModalVisible: false,
    });
  };
  showModalSort = () => {
    this.setState({
      isSortModal: true,
    });
  };
  renderItemSort = item => {
    const {filter} = this.state;
    return item ? (
      <View style={styles.containerSearchItem}>
        <TouchableOpacity
          style={styles.insideSortItem}
          onPress={() => this.handleSortFilter(item)}>
          <Text style={styles.titleSort}>{item.item.name}</Text>
          <Text>{item.item.value == filter && checkIcon}</Text>
        </TouchableOpacity>
      </View>
    ) : null;
  };

  handleCancel = () => {
    this.setState({
      isSortModal: false,
      filter: 'asc',
    });
  };
  handleSortFilter = item => {
    this.setState({
      filter: item.item.value,
    });
  };
  handelSortChange = async () => {
    const {selected, filter} = this.state;
    console.log('handelSortChange');
    if (selected || filter) {
      const result = await jalaServices.getAllShirmpPrices(
        '',
        filter || 'asc',
        (selected && selected.id) || '',
      );
      console.log('result', result);
      this.setState({
        data: result.data,
        isModalVisible: false,
        isSortModal: false,
      });
    }
  };
  render() {
    const {data, value, filter, location, loading} = this.state;
    const sort = [
      {
        id: 1,
        name: 'tardekat',
        value: 'neast',
      },
      {
        id: 2,
        name: 'termurah',
        value: 'asc',
      },
      {
        id: 3,
        name: 'termahal',
        value: 'desc',
      },
    ];
    if (loading) {
      return (
        <View style={styles.contentNotFound}>
          <ActivityIndicator size="large" color="#0050ac" />
        </View>
      );
    }
    return (
      <View>
        <ScrollView>
          <View style={styles.locationView}>
            <Text>your location: </Text>
            <Text>
              {(this.props.address &&
                this.props.address.plus_code &&
                this.props.address.plus_code.compound_code) ||
                ''}
            </Text>
          </View>
          {data && data.length > 0 ? (
            <FlatList
              style={styles.container}
              data={data}
              renderItem={this.renderItem}
              keyExtractor={extractKey}
              extraData={this.state}
            />
          ) : (
            <View style={styles.contentNotFound}>
              {infoIcon}
              <Text>not found data</Text>
            </View>
          )}
        </ScrollView>
        <View style={styles.footer}>
          <View style={styles.footerLeft}>
            <TouchableOpacity
              style={styles.insideLeft}
              onPress={this.toggleModal}>
              <View>{fiterIcon}</View>
              <View style={{marginLeft: 5}}>
                <Text style={styles.txtFilter}>Filter Lokasi</Text>
                <Text style={styles.txtFilterCurrent}>
                  {data.length || 0} filter diterapkan
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.footerRight}>
            <TouchableOpacity
              style={styles.insideLeft}
              onPress={this.showModalSort}>
              <View>{sortIcon}</View>
              <View style={{marginLeft: 5}}>
                <Text style={styles.txtFilter}>Urutkan</Text>
                <Text style={styles.txtFilterCurrent}>{filter || 'desc'}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View>
          <Modal
            isVisible={this.state.isModalVisible}
            // swipeDirection={['up', 'left', 'right', 'down']}
            style={styles.modalcontainer}>
            <KeyboardAvoidingView
              behavior={Platform.OS === 'ios' ? 'padding' : null}
              enabled
              style={{flex: 1}}>
              <View style={styles.modalContent}>
                <View style={styles.sortView}>
                  <View style={styles.footerSort}>
                    <TouchableOpacity
                      onPress={this.resetFilter}
                      style={styles.btnSort}>
                      <Text style={styles.btnText}>Reset Filter</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.headSort}>
                    <View style={styles.btnView}>
                      <View style={styles.btnLeft}>
                        <TextInput
                          value={value}
                          style={styles.input}
                          onChangeText={e => this.onChangeText(e)}
                          keyboardType={'default'}
                        />
                        <View style={{marginTop: 20, marginRight: 10}}>
                          {searchIcon}
                        </View>
                      </View>
                      <TouchableOpacity onPress={this.changeFilter}>
                        <Text>Bata</Text>
                      </TouchableOpacity>
                    </View>
                    <ScrollView>
                      <Text style={styles.txtLocation}>Piih Daerah:</Text>
                      <FlatList
                        style={styles.localtionContainer}
                        data={location}
                        renderItem={this.renderItemSearch}
                        keyExtractor={extractKey}
                        extraData={this.state}
                      />
                    </ScrollView>
                  </View>
                </View>
              </View>
            </KeyboardAvoidingView>
          </Modal>
          <Modal
            isVisible={this.state.isSortModal}
            // swipeDirection={['up', 'left', 'right', 'down']}
            style={styles.modalcontainer}>
            <View style={styles.modalContent}>
              <View style={styles.sortViewFilter}>
                <View style={styles.sortFooter}>
                  <TouchableOpacity
                    onPress={this.handelSortChange}
                    style={styles.btnSortFilter}>
                    <Text style={styles.btnTextSort}>Unrutkan</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.sortHeader}>
                  <TouchableOpacity onPress={this.handleCancel}>
                    <Text
                      style={{
                        textAlign: 'right',
                        color: '#c9ccd2',
                        paddingRight: 5,
                      }}>
                      batal
                    </Text>
                  </TouchableOpacity>

                  {sort && (
                    <FlatList
                      style={styles.sortContainer}
                      data={sort}
                      renderItem={this.renderItemSort}
                      keyExtractor={extractKey}
                      extraData={this.state}
                    />
                  )}
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentNotFound: {
    justifyContent: 'center',
    alignItems: 'center',
    height: deviceHeight,
  },
  row: {
    padding: 15,
    marginBottom: 5,
  },
  title: {
    backgroundColor: '#eaeff5',
    color: '#bdc8de',
    padding: 10,
    fontSize: 16,
  },
  content: {
    flex: 1,
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  price: {
    fontSize: 25,
    fontWeight: '500',
  },
  address: {
    fontSize: 12,
    paddingTop: 5,
    color: '#1b77df',
    fontWeight: '500',
  },
  location: {
    marginTop: 10,
    color: '#bfcadf',
  },
  left: {},
  right: {},
  detail: {
    color: '#bfcadf',
  },
  next: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    marginTop: 30,
  },
  containerItem: {
    borderBottomColor: '#d9e1ed',
    borderBottomWidth: 1.5,
  },
  footer: {
    // backgroundColor: 'red',
    // // position: 'absolute'
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    // position: 'absolute',
    // left: 0,
    // bottom: 0,
    width: '100%',
    height: 100,
    flexDirection: 'row',
    justifyContent: 'space-between',
    position: 'absolute', //Here is the trick
    bottom: 0, //Here is the trick
  },
  footerLeft: {
    flex: 1,
    backgroundColor: '#0050ac',
    alignItems: 'center',
  },
  footerRight: {
    flex: 1,
    backgroundColor: '#1b77df',
    alignItems: 'center',
  },
  modalcontainer: {
    margin: 0,
  },
  modalContent: {
    // alignItems: 'center',
    // backgroundColor: '#1b77df',
    // height: deviceHeight / 2,
    // position: 'absolute',
    // left: 0,
    // bottom: 0,
    // right: 0,
    width: '100%',
    height: deviceHeight / 2,
    // height: 400,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#1b77df',
    position: 'absolute', //Here is the trick
    bottom: 0, //Here is the trick
  },
  btnSort: {
    alignItems: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: deviceWidth / 1.2,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#fff',
  },
  btnText: {
    color: '#fff',
  },
  sortView: {
    flex: 1,
    flexDirection: 'column-reverse',
    alignContent: 'flex-end',
    marginBottom: 0,
  },
  footerSort: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0050ac',
  },
  input: {
    height: 50,
    flex: 1,
    color: '#4b82c4',
    borderRadius: 2,
    marginTop: 10,
  },
  btnView: {
    margin: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomColor: '#333',
    borderBottomWidth: 0.2,
  },
  btnLeft: {
    flexDirection: 'row',
    flex: 1,
    alignContent: 'center',
    alignSelf: 'center',
    backgroundColor: '#0050ac',
  },
  headSort: {
    flex: 8,
  },
  txtLocation: {
    padding: 10,
    color: '#fff',
  },
  localtionContainer: {
    flex: 1,
  },
  containerSearchItem: {
    // backgroundColor: '#0050ac',
    padding: 5,
  },
  titleSearch: {
    color: '#fff',
    textTransform: 'uppercase',
    fontWeight: '600',
    fontSize: 16,
    opacity: 0.5,
  },
  insideLeft: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtFilter: {
    color: '#fff',
    fontSize: 20,
  },
  txtFilterCurrent: {
    color: '#fff',
    fontSize: 10,
  },
  locationView: {
    flex: 2,
    padding: 10,
    backgroundColor: '#fff',
  },
  sortContainer: {
    paddingLeft: 10,
    flex: 1,
  },
  sortFooter: {
    alignItems: 'center',
    flex: 2,
    backgroundColor: '#0050ac',
    justifyContent: 'center',
  },
  sortHeader: {
    paddingTop: 10,
    flex: 8,
  },
  btnSortFilter: {
    alignItems: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: deviceWidth / 1.2,
    borderRadius: 5,
    backgroundColor: '#fff',
  },
  sortViewFilter: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column-reverse',
    marginBottom: 0,
  },
  btnTextSort: {
    color: '#0050ac',
  },
  insideSortItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 0.5,
    borderBottomColor: '#fff',
    paddingTop: 5,
    paddingBottom: 5,
  },
  titleSort: {
    color: '#fff',
  },
});
const mapStateToProps = state => {
  console.log('rootReducer', state);
  return {
    location: state.rootReducer.location,
    address: state.rootReducer.address,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setItem: item => dispatch(setItem(item)),
    setLocation: item => dispatch(setLocation(item)),
    setAddress: item => dispatch(setAddress(item)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);
