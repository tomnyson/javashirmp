const formatNumber = number =>
  number && number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
// new Intl.NumberFormat('en-IN', {maximumSignificantDigits: 3}).format(number);

module.exports = {
  formatNumber: formatNumber,
};
