import React from 'react';
import {View, Text} from 'react-native';
import {Router, Scene, Actions} from 'react-native-router-flux';
class SplashScreen extends React.Component {
  constructor(props) {
    super(props);
  }
  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
    const data = await this.performTimeConsumingTask();

    if (data !== null) {
      Actions.home();
    }
  }
  performTimeConsumingTask = async () => {
    return new Promise(resolve =>
      setTimeout(() => {
        resolve('result');
      }, 2000),
    );
  };

  render() {
    return (
      <View style={styles.viewStyles}>
        <Text style={styles.textStyles}>JALA</Text>
      </View>
    );
  }
}

const styles = {
  viewStyles: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0050ac',
  },
  textStyles: {
    color: 'white',
    fontSize: 40,
    fontWeight: 'bold',
  },
};

export default SplashScreen;
