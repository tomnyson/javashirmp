import queryString from 'query-string';

const method = {
  GET: 'GET',
  POST: 'POST',
};
const domain = 'https://app.jala.tech/';
const token =
  'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU1ZGI1ZjY0MjRjYTdkYjIzZjA0MTYwMmE5MWFiOGU4YTgyYTMwMjZiYjlhMGEwZWIwNjM1NmE2MmJhOWVjNjEwZGI0ZDM1MDBlZGVlMzg1In0.eyJhdWQiOiIzIiwianRpIjoiZTVkYjVmNjQyNGNhN2RiMjNmMDQxNjAyYTkxYWI4ZThhODJhMzAyNmJiOWEwYTBlYjA2MzU2YTYyYmE5ZWM2MTBkYjRkMzUwMGVkZWUzODUiLCJpYXQiOjE1NzQ1Njk4MjYsIm5iZiI6MTU3NDU2OTgyNiwiZXhwIjoxNTc1ODY1ODI1LCJzdWIiOiIiLCJzY29wZXMiOltdfQ.ea8i-SzqtqyjcPAgLbqB6K_PZ_d8YcplR7vH8M8ALFyKbgdcIAoABxsl2nT8o6JbDCUm8hIFsPuQQsoDox5Pw5X0vKDACUwfPJT0JF0vQVF0cA4zU59uSWqULZcbYPWTpR1w2IoPJSrftlQ-PZm-OKMc5vuuOIManq-LL2paS1jqMjiLnV6Lg99oyPOq_F182h-9trxB4PPSUV3_pED3cu4-wNSl1hWWLYSZ26Kdtp1hC-EV7CSMYVA0_4ZJzmCGToBK58dED8U9CnRVa9_DcPbo7tX12b74XCvP3xwCacbWjnpp12Y8W9h7TU0hRjT3KAC-G-5VA0ZkJCvKgKrHjNensJxIb0O7KU-0dN_5LrdluvS01ONdgGZ27oIeLg6GIA_sPpT_8GGKOP4XQCABi2KsrohDagb7EMgK4uaOEEowDep-6Sq0-NrWoHx1laIOUSYnuhcJt_o6daS8CD6cp_Y53ff9CCsPSRYoWWW2fI7A-dONysd56iPF39p7Ys6Hptw6pThbX5SAvM-AT1oOfmasoyM9D3iz9Spjr7MTsV9r-fxRzjnR4zWffjFKcU0byXn7vS-zfLRTfN1KmSGk8B-rUDyJVZexvxrYFMXA1VffnKI_vLaPMaZ6ynGOm6V0ReQy_-5n2M2oG2mb4B4XgvCvsy0JFg0f53noqTXvn3w';

class JalaServices {
  static getToken = async () => {
    console.log('call this function');
    const url = `${domain}oauth/token`;
    const body = {
      grant_type: 'client_credentials',
      client_id: 3,
      client_secret: '8VrhzrEXjV5JcRfmz1pQ8AE1mDnISeZ8hbBqi56b',
      scope: '*',
    };
    const result = JalaServices.fetchApi(method.POST, url, body);
    console.log('result', result);
    return result;
  };

  static getAllShirmpPrices = async (
    name = '',
    sort = 'desc',
    regency_id = '',
  ) => {
    console.log('call this getAllShirmpPrices');
    const url = `${domain}api/shrimp_prices?search=${name}&with=creator,species,region&sort=size_100|creator.name,${sort}&region_id=${regency_id}&per_page=100`;
    console.log('url', url);
    const result = JalaServices.fetchApi(method.GET, url, null);
    console.log('shrimp_prices', result);
    return result;
  };
  static getSpeciesByID = async id => {
    console.log('call this getAllShirmpPrices');
    const url = `${domain}api/species/${id}`;
    console.log('domian', url);
    const result = JalaServices.fetchApi(method.GET, url, null);
    return result;
  };
  static getSearchLocation = async (name = '') => {
    const params = queryString.stringify({
      search: name,
      per_page: 100,
    });
    console.log('call this getAllShirmpPrices');
    const url = `${domain}api/regions?${params}`;
    const result = JalaServices.fetchApi(method.GET, url, null);
    console.log('regions', result);
    return result;
  };
  static getAddressByLocation = async (lang, long) => {
    const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lang},${long}&key=AIzaSyBQez5NzWDEPLrhocWTISs_cjR3sw5CIGA`;
    const result = JalaServices.fetchApi(method.GET, url, null);
    console.log('getAddressByLocation', result);
    return result;
  };
  static fetchApi = async (action, url, body) => {
    try {
      const result = await fetch(url, {
        method: action,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body:
          action === method.GET
            ? null
            : JSON.stringify({
                body,
              }),
      });
      return result.json();
    } catch (error) {
      console.log('error', error);
      throw error;
    }
  };
}
export default JalaServices;
