/* eslint-disable react/react-in-jsx-scope */
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

const backIcon = <Icon name="chevron-left" size={25} color="#ccc" />;
const nextIcon = <Icon name="chevron-right" size={15} color="#b6c2da" />;
const shareIcon = <Icon name="share-alt" size={25} color="#ccc" />;
const fiterIcon = <Icon name="filter" size={30} color="#b6c2da" />;
const sortIcon = <Icon name="sort" size={30} color="#b6c2da" />;
const searchIcon = <Icon name="search" size={20} color="#b6c2da" />;
const infoIcon = <Icon name="info" size={30} color="#b6c2da" />;
const checkIcon = <Icon name="check" size={20} color="#b6c2da" />;

module.exports = {
  backIcon,
  shareIcon,
  sortIcon,
  fiterIcon,
  nextIcon,
  searchIcon,
  infoIcon,
  checkIcon,
};
