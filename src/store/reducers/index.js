import {SET_ITEM, SET_LOCATION, SET_ADDRESS_LOCATION} from '../constants';

const initialState = {
  location: null,
  selected: null,
  address: null,
};
const countReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ITEM:
      return {
        ...state,
        item: action.payload,
      };
    case SET_LOCATION:
      return {
        ...state,
        location: action.payload,
      };
    case SET_ADDRESS_LOCATION:
      return {
        ...state,
        address: action.payload,
      };
    default:
      return state;
  }
};
export default countReducer;
