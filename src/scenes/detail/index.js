/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Button,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {Router, Scene, Actions} from 'react-native-router-flux';
import {VictoryLine, VictoryChart, VictoryLabel} from 'victory-native';
import {formatNumber} from '../../utils';

const Detail = props => {
  const detail = props.data || null;
  const listSize = [
    'size_20',
    'size_30',
    'size_40',
    'size_50',
    'size_60',
    'size_70',
    'size_80',
    'size_90',
    'size_100',
    'size_110',
    'size_120',
    'size_120',
    'size_130',
    'size_140',
    'size_150',
    'size_160',
    'size_170',
    'size_180',
    'size_190',
    'size_200',
  ];
  const data = [
    [
      [0, 1],
      [1, 3],
      [3, 7],
      [4, 9],
    ],
  ];
  const categories = [];
  const priceData = [];
  listSize.map((item, index) => {
    const check = detail.hasOwnProperty(item);
    if (check && detail[item] !== null && categories.length <= 10) {
      categories.push(item.split('_')[1]);
      priceData.push({
        x: index,
        y: detail[item],
      });
    }
  });
  return (
    <ScrollView>
      {detail ? (
        <View style={styles.container}>
          <View style={styles.specsies}>
            <Text style={styles.specsiesName}>
              specsies: {detail.species.data.name}{' '}
            </Text>
            <Text style={styles.specsiesAlias}>
              {detail.species.data.aliases}{' '}
            </Text>
          </View>
          <View style={styles.listPice}>
            {listSize.map(item => {
              //check item exist
              const check = detail.hasOwnProperty(item);
              if (check && detail[item] !== null) {
                return (
                  <View style={styles.detailSize}>
                    <Text>Haga ukuran {item.split('_')[1]}</Text>
                    <Text>RB {formatNumber(detail[item])}</Text>
                  </View>
                );
              }
            })}
          </View>
          <View style={styles.containerChart}>
            <View style={styles.titleChart}>
              <Text style={styles.headTitle}>
                Peckempangan harga (ukuran 100)
              </Text>
            </View>
            <VictoryChart>
              <VictoryLine
                categories={{x: categories}}
                interpolation="natural"
                style={{
                  data: {stroke: '#4a93e5'},
                  parent: {border: '1px solid #ccc'},
                }}
                data={priceData}
                labels={({datum}) => {
                  console.log('datum.x ', datum.x == 1);
                  return datum.x == categories.length - 1
                    ? 'rb ' + formatNumber(datum.y)
                    : null;
                }}
                labelComponent={<VictoryLabel renderInPortal dy={-20} />}
                animate={{
                  duration: 1000,
                  onLoad: {duration: 1000},
                }}
              />
            </VictoryChart>
          </View>
          <View style={styles.specsies}>
            <Text style={styles.titleContent}>catata:</Text>
            <Text>{detail.remark || ''}</Text>
          </View>
          <View style={[styles.specsies, styles.note]}>
            <Text style={styles.titleContent}>Diedit pada:</Text>
            <Text>
              {detail.creator &&
                new Date(detail.creator.created_at).toDateString()}
              {', '}
              oleh {detail.creator && detail.creator.gender_name}
            </Text>
          </View>
        </View>
      ) : (
        <View>
          <Text>Loading...</Text>
        </View>
      )}
    </ScrollView>
  );
};
export default Detail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#eaeff5',
  },
  specsies: {
    marginTop: 10,
    marginBottom: 10,
    padding: 20,
    backgroundColor: '#fff',
  },
  specsiesName: {
    color: '#d0d3d8',
  },
  specsiesAlias: {
    fontSize: 16,
    marginTop: 10,
    color: '#1b77df',
    fontWeight: '700',
  },
  detailSize: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    paddingTop: 15,
    paddingBottom: 15,
    marginBottom: 1,
    marginTop: 1,
    backgroundColor: '#fff',
  },
  containerChart: {
    flex: 1,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5fcff',
  },
  titleChart: {
    alignSelf: 'flex-start',
    padding: 10,
  },
  headTitle: {
    fontSize: 15,
    color: '#828996',
    fontWeight: '700',
  },
  titleContent: {
    color: '#828996',
  },
  note: {
    marginTop: 0,
  },
});
