import {SET_LOCATION, SET_ITEM, SET_ADDRESS_LOCATION} from '../constants';

export const setAddress = address => {
  return {
    type: SET_ADDRESS_LOCATION,
    payload: address,
  };
};
export function setLocation(location) {
  return {
    type: SET_LOCATION,
    payload: location,
  };
}
export function setItem(item) {
  return {
    type: SET_ITEM,
    payload: item,
  };
}
