import {createStore, combineReducers, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import countReducer from './reducers';

const rootReducer = combineReducers({rootReducer: countReducer});
const logger = createLogger({
  ReduxThunk,
});
/* eslint-disable no-underscore-dangle */
const configureStore = () => {
  return createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
      window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(logger),
  );
};
/* eslint-enable */
export default configureStore;
